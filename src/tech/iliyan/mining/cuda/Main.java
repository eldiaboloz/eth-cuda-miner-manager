package tech.iliyan.mining.cuda;

import java.io.PrintStream;
import java.util.prefs.Preferences;

public class Main {

    public static void main(String[] args) {
        try {
            CliArgs cliArgs = new CliArgs(args);
            System.out.println(cliArgs);
            HttpResponder resp = new HttpResponder(cliArgs);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

public static boolean isAdmin(){
    Preferences prefs = Preferences.systemRoot();
    PrintStream systemErr = System.err;
    synchronized(systemErr){    // better synchroize to avoid problems with other threads that access System.err
        System.setErr(null);
        try{
            prefs.put("foo", "bar"); // SecurityException on Windows
            prefs.remove("foo");
            prefs.flush(); // BackingStoreException on Linux
            return true;
        }catch(Exception e){
            return false;
        }finally{
            System.setErr(systemErr);
        }
    }
}

}
