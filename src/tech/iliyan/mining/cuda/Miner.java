package tech.iliyan.mining.cuda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;
import java.util.regex.Pattern;

public class Miner {
    public CliArgs cliArgs = null;

    public Miner(CliArgs args) {
        this.cliArgs = args;
    }

    public Miner() {
        this(new CliArgs());
    }

    public boolean start() throws IOException {
        Vector<String> cmd = new Vector<String>(10);
        // miner cmd ( tested with ethminer ) - fullpath or only executable
        cmd.add(cliArgs.minerCmd);
        // cuda
        cmd.add("-U");
        // pool address ( tested with dwarfpool )
        cmd.add("-F");
        StringBuilder poolSb = new StringBuilder(100);
        poolSb.append(cliArgs.poolAddress);
        poolSb.append("/" + cliArgs.workerWallet);
        if (cliArgs.workerName != "") {
            poolSb.append("/" + cliArgs.workerName);
        }
        if (cliArgs.workerEmail != "") {
            poolSb.append("/" + cliArgs.workerEmail);
        }
        cmd.add(poolSb.toString());
        if (cliArgs.clGlobalWork != "") {
            cmd.add("--cl-global-work");
            cmd.add(cliArgs.clGlobalWork);
        }
        if (cliArgs.clLocalWork != "") {
            cmd.add("--cl-local-worк");
            cmd.add(cliArgs.clLocalWork);
        }
        for (String temp : cliArgs.minerArgs) {
            cmd.add(temp);
        }
        Vector<String[]> cmds = new Vector<String[]>(1);
        if (cliArgs.gpuGroups.size() > 0) {
            // we have gpu groups so we will need more than 1 process
            for (String gpuGroup : cliArgs.gpuGroups) {
                Vector<String> buff = (Vector<String>) cmd.clone();
                buff.add("--cuda-devices");
                for (String gpuGroupT : gpuGroup.split(Pattern.quote(","))) {
                    // ethminer expects each device as seperate arg after --cuda-devices
                    buff.add(gpuGroupT.trim());
                }
                cmds.add(buff.toArray(new String[buff.size()]));
            }
        } else {
            cmds.add(cmd.toArray(new String[cmd.size()]));
        }
        for (String[] proc : cmds) {
            this.create(proc);
        }
        return true;
    }

    protected boolean create(String[] cmd) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(cmd);
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while (true) {
            line = r.readLine();
            if (line == null) {
                break;
            }
        }
        return true;
    }
}
