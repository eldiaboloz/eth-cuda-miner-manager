package tech.iliyan.mining.cuda;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class HttpResponder {
    protected CliArgs cliArgs = null;
    public HttpResponder(CliArgs cliArgs) throws Exception {
        this.cliArgs = cliArgs;
        Integer port = cliArgs.port;
        if (port < 0 || port > 65535) {
            throw new Exception("Port " + port.toString() + " is not in valid range !");
        }
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
//        server.createContext("/gpu", new ReadGpuHandler());
        server.createContext("/", new GenericHandler());
        server.start();
    }

    protected void handleRoutes(String[] params) {
        switch (params[0]) {
            case "config":
                break;
        }

    }

    static class GenericHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            System.out.println("new request from " + t.getRemoteAddress().toString());
            System.out.println(t.getRequestURI());
            String response = "This is the response";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
//            Runnable r = new Runnable() {
//                public void run() {
//                    runYourBackgroundTaskHere();
//                }
//            };
        }
    }

    static class ReadGpuHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            System.out.println("new request from " + t.getRemoteAddress().toString());
            Collect cl = new Collect();
            String response = "";
            StringBuilder sb = new StringBuilder(500);
            for (Gpu gpu : cl.fromTool()) {
                sb.append(gpu).append("\n");
            }
            response = sb.toString();
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }


}
