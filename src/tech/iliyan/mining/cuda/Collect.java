package tech.iliyan.mining.cuda;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Vector;

public class Collect {

    public Vector<Gpu> fromTool() throws java.io.IOException {
        Vector<Gpu> temp = new Vector<Gpu>(6);
        String pathToTool = "C:\\Program Files\\NVIDIA Corporation\\NVSMI\\nvidia-smi";
        if (System.getProperty("os.name").toLowerCase().startsWith("windows") == false) {
            // if we are not on windows we hope that nvidia-smi is in path
            pathToTool = "nvidia-smi";
        }
        ProcessBuilder builder = new ProcessBuilder(
                pathToTool,
                "--query-gpu=gpu_name,temperature.gpu,power.draw,fan.speed",
                "--format=csv,noheader,nounits"
        );
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while (true) {
            line = r.readLine();
            if (line == null) {
                break;
            }
            temp.add(new Gpu(line));
        }
        return temp;
    }
}
