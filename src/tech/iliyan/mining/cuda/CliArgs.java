package tech.iliyan.mining.cuda;

import java.util.Vector;
import java.util.regex.Pattern;

public class CliArgs {
    public String workerWallet = "0x7fbb4e9c2c1fd0c16d4bafc71dcaaa0171346caa";
    public String workerName = "javamonitor";
    public String workerEmail = "";
    public String poolAddress = "http://eth-eu.dwarfpool.com";
    public Integer port = 8888;
    public Vector<String> gpuGroups = new Vector<String>(0);
    public String minerCmd = "ethminer";
    public String clLocalWork = "128";
    public String clGlobalWork = "8192";
    public Vector<String> minerArgs = new Vector<String>(0);

    public CliArgs() {
    }

    public CliArgs(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i] == "") {
                continue;
            }
            String[] parts = args[i].split(Pattern.quote("="));
            if (parts.length != 2) {
                // something is wrong ignore this opt
                continue;
            }
            switch (parts[0].toLowerCase()) {
                case "worker-wallet":
                case "workerwallet": {
                    this.workerWallet = parts[1];
                    break;
                }
                case "worker-name":
                case "workername": {
                    this.workerName = parts[1];
                    break;
                }
                case "worker-email":
                case "workeremail": {
                    this.workerEmail = parts[1];
                    break;
                }
                case "cl-local-work":
                case "cllocalwork": {
                    this.clLocalWork = parts[1];
                    break;
                }
                case "cl-global-work":
                case "clglobalwork": {
                    this.clGlobalWork = parts[1];
                    break;
                }
                case "pool-wallet":
                case "pooladdress": {
                    this.poolAddress = parts[1];
                    break;
                }
                case "port": {
                    this.port = Integer.parseInt(parts[1]);
                    break;
                }
                case "miner-cmd":
                case "minercmd": {
                    this.minerCmd = parts[1];
                    break;
                }
                case "miner-args":
                case "minerargs": {
                    this.minerArgs.add(parts[1]);
                    break;
                }
                case "gpu-groups":
                case "gpugroups": {
                    this.gpuGroups.add(parts[1]);
                    break;
                }
            }
        }
    }

    public String toString() {
        return Dumper.dump(this);
    }
}
