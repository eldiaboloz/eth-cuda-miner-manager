package tech.iliyan.mining.cuda;

import java.lang.reflect.Field;

public class Dumper {
    // inspired by https://stackoverflow.com/a/17095665
    public static String dump(Object o) {
        StringBuilder sb = new StringBuilder(500);
        for (Field f : o.getClass().getDeclaredFields()) {
            Class t = f.getType();
            Object v = null;
            try {
                v = f.get(o);
                sb.append(f.getName() + ": " + v.toString());
                sb.append("\n");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return sb.toString();
    }
}
