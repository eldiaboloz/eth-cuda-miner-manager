package tech.iliyan.mining.cuda;

import java.util.regex.Pattern;

public class Gpu {
    public String name = "";
    public Integer temp = 0;
    public Float powerDraw = 0.0f;
    public Integer fanSpeed = 0;

    public Gpu(String line) throws java.io.IOException {
        String parts[] = line.split(Pattern.quote(","));
        if (parts.length != 4) {
            throw new java.io.IOException("Unknown input format!");
        }
        this.name = parts[0].trim();
        this.temp = Integer.parseInt(parts[1].trim());
        this.powerDraw = Float.parseFloat(parts[2].trim());
        this.fanSpeed = Integer.parseInt(parts[3].trim());
    }

    @Override
    public String toString() {
        return (
                this.name + ", " + this.temp.toString() + ", " + this.powerDraw.toString() + ", " + this.fanSpeed.toString()
        );
    }
}
